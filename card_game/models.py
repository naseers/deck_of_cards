# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .constants import SUITS, VALUES

import random
import string

# Create your models here.

class Card():
    suit = None
    value = None

    def __init__(self, suit='', value='', code=''):
        if code:
            self.value = code[0]
            for suit in SUITS:
                if suit[0] == code[1]:
                    self.suit = suit
        else:
            self.suit = suit
            self.value = value

    def code(self):
        return self.value + self.suit[0]

    def image(self):
        return "https://deckofcardsapi.com/static/img/"+self.code()+".png"


class Pile():
    name = ''
    cards = []

    def __init__(self, name, cards):
        self.name = name
        self.cards = cards

    def remaining(self):
        return len(self.cards)

    def shuffle(self):
        random.shuffle(self.cards)

    def draw(self, count=0, codes=''):
        cards = []
        while count >= 1:
            cards.append(self.cards.pop())
            count = count - 1
        if codes:
            index = 0
            for card in self.cards:
                if card.code() in codes:
                    cards.append(self.cards.pop(index))
                index = index + 1
        return cards

class Deck():
    id = None
    cards = []
    piles = []

    def __init__(self, cards=[]):
        self.id = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(12))
        self.cards = []
        self.piles = []
        if cards:
            self.cards = cards
        else:
            for suit in SUITS:
                for value in VALUES:
                    self.cards.append(Card(suit=suit, value=value))

    def shuffle(self):
        random.shuffle(self.cards)

    def remaining(self):
        return len(self.cards)

    def draw(self, count=1):
        cards = []
        while count >= 1:
            cards.append(self.cards.pop())
            count = count - 1
        return cards

    def addPile(self, pile):
        self.piles.append(pile)