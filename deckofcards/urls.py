"""deckofcards URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from card_game import views

urlpatterns = [
    # url(r'api/deck^', include('card_game.urls')),
    url(r'^api/deck/new/shuffle/$', views.newShuffle),
    url(r'^api/deck/(?P<deck_id>[\w]+)/draw/$', views.draw),
    url(r'^api/deck/(?P<deck_id>[\w\d\-]+)/shuffle/$', views.shuffle),
    url(r'^api/deck/(?P<deck_id>[\w\d\-]+)/pile/(?P<pile_name>[\w\-]+)/add/$', views.addToPile),
    url(r'^api/deck/(?P<deck_id>[\w\d\-]+)/pile/(?P<pile_name>[\w\-]+)/shuffle/$', views.shufflePile),
    url(r'^api/deck/(?P<deck_id>[\w\d\-]+)/pile/(?P<pile_name>[\w\-]+)/list/$', views.listPile),
    url(r'^api/deck/(?P<deck_id>[\w\d\-]+)/pile/(?P<pile_name>[\w\-]+)/draw/$', views.drawPile),
    url(r'^api/deck/(?P<deck_id>[\w\d\-]+)/draw/bottom/$', views.draw),
    url(r'^api/deck/new/$', views.new),
]
