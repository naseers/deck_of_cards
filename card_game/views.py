# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.core.cache import cache

import json
from .models import Deck, Card, Pile
from .constants import GAME_KEY

# Create your views here.

def updateCache(updatedDeck):
    decks = cache.get(GAME_KEY)
    index = 0
    for deck in decks:
        if deck.id == updatedDeck.id:
            decks.pop(index)
        index = index + 1
    decks.append(updatedDeck)
    cache.set(GAME_KEY, decks, 3600)

def getDeck(deck_id):
    decks = cache.get(GAME_KEY)
    targetDeck = None
    for deck in decks:
        if deck.id == deck_id:
            targetDeck = deck
    return targetDeck

def getCardsFromCodes(cards):
    cardArray = []
    cardCodes = cards.split(',')
    for cardCode in cardCodes:
        card = Card(code=cardCode)
        cardArray.append(card)
    return cardArray

def getPileFromDock(targetDeck, pileName):
    linkedPiles = targetDeck.piles
    pile = None
    for linkedPile in linkedPiles:
        if linkedPile.name == pileName:
            pile = linkedPile
    return pile

def draw(request, deck_id):
    result = {}
    try:
        count = int(request.GET.get('count', 1))
        result['deck_id'] = deck_id
        targetDeck = getDeck(deck_id)
        if not targetDeck:
            result['success'] = False
        else:
            result['cards'] = []
            cards = targetDeck.draw(count)
            updateCache(targetDeck)
            for card in cards:
                result['cards'].append(card.__dict__)
            result['remaining'] = targetDeck.remaining()
            result['success'] = True
    except:
        result['success'] = False
    return HttpResponse(json.dumps(result))

def new(request):
    result = {}
    try:
        deck = Deck()
        cache.set(GAME_KEY, [deck], 3600)
        result['success'] = True
        result['shuffled'] = False
        result['deck_id'] = deck.id
        result['remaining'] = deck.remaining()
    except:
        result['success'] = False
    return HttpResponse(json.dumps(result))


def shuffle(request, deck_id):
    result = {}
    result['deck_id'] = deck_id
    try:
        targetDeck = getDeck(deck_id)
        if targetDeck:
            targetDeck.shuffle()
            updateCache(targetDeck)
            result['success'] = True
            result['shuffled'] = True
            result['remaining'] = targetDeck.remaining()
        else:
            result['success'] = False
            result['shuffled'] = False
    except:
        result['success'] = False
    return HttpResponse(json.dumps(result))

def newShuffle(request):
    if request.method == 'POST':
        count = int(request.POST.get('deck_count', 1))
        cards = request.POST.get('cards', '')
    else:
        count = int(request.GET.get('deck_count', 1))
        cards = request.GET.get('cards', '')

    result = {}

    try:
        decks = []
        if cards:
            cardArray = getCardsFromCodes(cards)
            decks.append(Deck(cardArray))
        else:
            while count >= 1:
                deck = Deck()
                deck.shuffle()
                decks.append(deck)
                count = count -1

        cache.set(GAME_KEY, decks, 3600)
        result['decks'] = []

        for deck in decks:
            result['decks'].append({'deck_id': deck.id, 'remaining': deck.remaining()})

        result['success'] = True
        result['shuffled'] = True
    except:
        result['success'] = False

    return HttpResponse(json.dumps(result))

def addToPile(request, deck_id, pile_name):
    result = {}
    try:
        cards = request.GET.get('cards', '')
        targetDeck = getDeck(deck_id)
        pile = getPileFromDock(targetDeck, pile_name)
        if not pile:
            pile = Pile(pile_name, getCardsFromCodes(cards))
            targetDeck.addPile(pile)
        else:
            cards = getCardsFromCodes(cards)
            pile.cards = pile.cards + cards
            targetDeck.pile = pile

        updateCache(targetDeck)
        result['success'] = True
        result['deck_id'] = deck_id
        result['remaining'] = targetDeck.remaining()
        result['piles'] = {pile_name:{"remaining":pile.remaining()}}
    except:
        result['success'] = False

    return HttpResponse(json.dumps(result))


def shufflePile(request, deck_id, pile_name):
    result = {}
    try:
        targetDeck = getDeck(deck_id)
        pile = getPileFromDock(targetDeck, pile_name)
        pile.shuffle()
        result['success'] = True
        result['deck_id'] = deck_id
        result['remaining'] = targetDeck.remaining()
        result['piles'] = {pile_name: {"remaining": pile.remaining()}}
    except:
        result['success'] = False

    return HttpResponse(json.dumps(result))


def listPile(request, deck_id, pile_name):
    result = {}
    try:
        targetDeck = getDeck(deck_id)
        pile = getPileFromDock(targetDeck, pile_name)
        pileStructure = {}
        for linkedPile in targetDeck.piles:
            if linkedPile == pile:
                cardArray = []
                for card in linkedPile.cards:
                    cardArray.append(card.__dict__)
                pileStructure[linkedPile.name] = {'cards': cardArray, 'remaining': linkedPile.remaining()}
            else:
                pileStructure[linkedPile.name] = {'remaining':linkedPile.remaining()}
        result['success'] = True
        result['deck_id'] = deck_id
        result['remaining'] = targetDeck.remaining()
        result['piles'] = pileStructure
    except:
        result['success'] = False

    return HttpResponse(json.dumps(result))

def drawPile(request, deck_id, pile_name, cardString='', count=''):
    result = {}
    try:
        cardString = request.GET.get('cards', 1)
        count = request.GET.get('count', 1)
        targetDeck = getDeck(deck_id)
        pile = getPileFromDock(targetDeck, pile_name)
        cards = pile.draw(count, cardString)
        result['success'] = True
        result['deck_id'] = deck_id
        result['remaining'] = targetDeck.remaining()
        result['piles'] = {pile_name: {"remaining": pile.remaining()}}
        cardArray = []
        for card in cards:
            cardArray.append(card.__dict__)
        result['cards'] = cardArray
    except:
        result['success'] = False

    return HttpResponse(json.dumps(result))
